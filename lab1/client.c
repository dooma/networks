// Un client trimite unui server un sir de caractere si un caracter.
// Serverul va returna clientului toate pozitiile pe care caracterul primit se regaseste in sir.

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdlib.h>

int main() {
  int c, index;
  struct sockaddr_in server;
  char character[2];
  char line[255];
  uint16_t presence, length, number;

  c = socket(AF_INET, SOCK_STREAM, 0);
  if (c < 0) {
    printf("Error while creating socket\n");
    return 1;
  }

  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr("127.0.0.1");

  if (connect(c, (struct sockaddr *) &server, sizeof(server)) < 0) {
    printf("Error connecting to server\n");
    return 1;
  }

  printf("Line = ");
  scanf("%[^\n]s", line);

  printf("Character = ");
  scanf("%s", character);

  send(c, character, strlen(character)+1, 0);
  send(c, line, strlen(line), 0);

  recv(c, &length, sizeof(length), 0);
  length = ntohs(length);

  printf("The aparitions are: %d\n", length);

  for (index = 0; index < length; ++index) {
    recv(c, &number, sizeof(number), 0);
    number = ntohs(number);
    if (index < length - 1) {
      printf("%d, ", number);
    } else {
      printf("%d\n", number);
    }
  }

  close(c);
}
