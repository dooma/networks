import socket
import sys

def presence(line):
    freq = {}
    char_max = None
    char_max_number = 0

    for char in line:
        number = 0
        for char2 in line:
            if char == char2:
                number += 1
        if char_max_number < number:
            char_max_number = number
            char_max = char

    return (char_max, char_max_number)

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.bind(('localhost', 40002))
    except:
        print "Cannot bind socket"
        sock.close()
        sys.exit()

    sock.listen(5)

    while True:
        try:
            conn, addr = sock.accept()
            print "Connected by ", addr

            string = conn.recv(2048)

            values = presence(string)
            print values[0]
            conn.send(str(values[0]))
            conn.send(str(values[1]))
        except Exception as message:
            sock.close()
            sys.exit(0)

main()
