# Un client trimite unui server un sir de caractere si un caracter C. Serverul returneaza toate caracterele din sir care urmeaza dupa C

import socket

LOCATION = ("localhost", 40002)
LOCATION2 = ("localhost", 40003)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

line = raw_input("Sentence = ")

character = raw_input("Caracter = ")

sock.sendto(line, LOCATION)
sock.sendto(character, LOCATION)

data, attr = sock.recvfrom(1024)

print "Connected from ", attr
print data

sock.close()
