import socket
import sys
import time

LOCATION = ("localhost", 40002)
LOCATION2 = ("localhost", 40003)

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    sock2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    sock.bind(LOCATION)

    while True:
        try:
            string, addr = sock.recvfrom(1024)
            print "Connected by ", addr

            char, addr = sock.recvfrom(1024)

            values = string.split(char, 1)[1]
            print values
            time.sleep(1)
            sock.sendto(values, addr)
            time.sleep(1)
        except Exception as message:
            print message
            sys.exit(0)

main()
