import socket
import struct
LOCATION = ("localhost", 1234)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

line = raw_input("Line = ")
character = raw_input("Character = ")

sock.sendto(character, LOCATION)
sock.sendto(line, LOCATION)

aparitions = struct.unpack("!h", sock.recvfrom(1024)[0])[0]

print "Total aparitions are: ", aparitions

while aparitions:
    print struct.unpack("!h", sock.recvfrom(1024)[0])[0], " ",
    aparitions -= 1

sock.close()
