#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

int s;
uint16_t positions[1000];

uint16_t find_aparitions(char line[], char character[]) {
  uint16_t aparitions = 0, index;

  for (index = 0; index < strlen(line); ++index) {
    if (line[index] == character[0])  {
      positions[aparitions] = htons(index);
      aparitions += 1;
    }
  }

  return aparitions;
}

void sig_handler(int signo) {
  puts("Closing server");
  shutdown(s, SHUT_RDWR);
  close(s);
  exit(0);
}

int main() {
  struct sockaddr_in server, client;
  int c, l, index, client_size;

  // Prepare to close socket;
  signal(SIGINT, sig_handler);

  s = socket(AF_INET, SOCK_DGRAM, 0);
  if (s < 0) {
    printf("Eroare la crearea socketului server\n");
    return 1;
  }

  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;

  if (bind(s, (struct sockaddr *) &server, sizeof(server)) < 0) {
    printf("Eroare la bind\n");
    return 1;
  }

  client_size = sizeof(client);

  while (1) {
    char character[2], line[255];
    uint16_t aparitions;

    memset(&client, 0, client_size);

    recvfrom(s, character, sizeof(character), 0, (struct sockaddr *) &client, &client_size);
    recvfrom(s, line, sizeof(line), 0, (struct sockaddr *) &client, &client_size);

    aparitions = find_aparitions(line, character);

    aparitions = htons(aparitions);

    sendto(s, (const char *)&aparitions, sizeof(aparitions), 0, (struct sockaddr *) &client, client_size);

    printf("I sent: %d\n", ntohs(aparitions));

    for (index = 0; index < aparitions; ++index) {
      sendto(s, (const char *)&positions[index], sizeof(positions[index]), 0, (struct sockaddr *) &client, client_size);
    }
   }
}
