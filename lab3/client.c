// Un client trimite unui server un sir de caractere si un caracter.
// Serverul va returna clientului toate pozitiile pe care caracterul primit se regaseste in sir.

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdlib.h>

int main() {
  int c, index, server_size;
  struct sockaddr_in server;
  char character[2];
  char line[255];
  uint16_t presence, length, number;

  c = socket(AF_INET, SOCK_DGRAM, 0);
  if (c < 0) {
    printf("Error while creating socket\n");
    return 1;
  }

  memset(&server, 0, sizeof(server));
  server.sin_port = htons(1234);
  server.sin_family = AF_INET;
  // server.sin_addr.s_addr = inet_addr("127.0.0.1");

  printf("Line = ");
  scanf("%[^\n]s", line);

  printf("Character = ");
  scanf("%s", character);

  server_size = sizeof(server);

  sendto(c, character, strlen(character)+1, 0, (struct sockaddr *) &server, server_size);
  sendto(c, line, strlen(line)+1, 0, (struct sockaddr *) &server, server_size);

  recvfrom(c, &length, sizeof(length), 0, (struct sockaddr *) &server, &server_size);
  length = ntohs(length);

  printf("The aparitions are: %d\n", length);

  for (index = 0; index < length; ++index) {
    recvfrom(c, &number, sizeof(number), 0, (struct sockaddr *) &server, &server_size);
    number = ntohs(number);
    if (index < length - 1) {
      printf("%d, ", number);
    } else {
      printf("%d\n", number);
    }
  }

  close(c);
}
